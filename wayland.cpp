/// Wayland
#include <wayland-client.h>
#include <wayland-cursor.h>

/// Pulseaudio
#include <pulse/simple.h>
#include <pulse/error.h>

/// Linux specific
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <linux/input.h>
#include <poll.h>		

/// C Standard library
#include <cstdlib>
#include <cstddef>
#include <cmath>

/// C++ Standard Library
#include <iostream>
#include <vector>
#include <utility>
#include <unordered_set>
#include <chrono>

#define internal static

const float PI = 3.14159265358979f;


internal wl_compositor *compositor;
internal wl_shell *shell;
internal wl_shm *shm;

/// Wayland probably pings us to check if we are alive. Not sure about
/// though
internal void
handle_ping(void *data, struct wl_shell_surface *shell_surface,
							uint32_t serial)
{
    wl_shell_surface_pong(shell_surface, serial);
    std::cout << "Ponged\n";
    //    std::cout << "Data: " << reinterpret_cast<const char*>(data) << '\n';
}

internal void
handle_configure(void *data, struct wl_shell_surface *shell_surface,
		 uint32_t edges, int32_t width, int32_t height)
{
    
}

internal void
handle_popup_done(void *data, struct wl_shell_surface *shell_surface)
{
}

internal const struct wl_shell_surface_listener shell_surface_listener = {
	handle_ping,
	handle_configure,
	handle_popup_done
};

internal void shm_format(void *data, struct wl_shm *wl_shm, uint32_t format) {
    std::cout << "Format: " << format << '\n';
}

internal const wl_shm_listener shm_listener = {
    shm_format
};

internal wl_pointer *pointer;
internal wl_cursor *default_cursor;
internal wl_surface *cursor_surface;

internal void pointer_enter(
    void *data, wl_pointer *pointer, uint32_t serial, wl_surface *surface,
    wl_fixed_t sx, wl_fixed_t sy) {
    std::cout << "Pointer entered surface at "
	      << wl_fixed_to_double(sx) << ' ' << wl_fixed_to_double(sy)
	      << '\n';

    wl_cursor_image *image = default_cursor->images[0];
    wl_buffer *buffer = wl_cursor_image_get_buffer(image);
    wl_pointer_set_cursor(
	pointer, serial, cursor_surface, image->hotspot_x, image-> hotspot_y
    );

    wl_surface_attach(cursor_surface, buffer, 0, 0);
    wl_surface_damage(cursor_surface, 0, 0, image->width, image->height);
    wl_surface_commit(cursor_surface);
}

internal void pointer_leave(
    void *data, wl_pointer *pointer, uint32_t serial, wl_surface *surface
) {
    std::cout << "Pointer left surface \n"; 
}

internal void pointer_moved(
    void *data, wl_pointer *pointer, uint32_t time,
    wl_fixed_t sx, wl_fixed_t sy
) {
    // std::cout << "Pointer moved to: " << wl_fixed_to_double(sx) << ' '
    // 	      << wl_fixed_to_double(sy) << '\n';
}

internal wl_shell_surface *shell_surface;
internal wl_seat *seat;

internal void pointer_button_pressed(
    void *data, wl_pointer *pointer, uint32_t serial, uint32_t time,
    uint32_t button, uint32_t state
) {
    std::cout << "Pointer Button pressed \n";

    if (button == BTN_LEFT && state == WL_POINTER_BUTTON_STATE_PRESSED)
	wl_shell_surface_move(shell_surface,
			      seat, serial);
}

internal void mouse_scrolled(
    void *data, wl_pointer *wl_pointer, uint32_t time,
    uint32_t axis, wl_fixed_t value) {
    /// axis is useless. the value is positive if wheel is scrolled
    /// down, otherwise, it is negative
    std::cout << "Pointer scrolled " << wl_fixed_to_double(value)
	      << " in axis " << axis << '\n';
}

internal const struct wl_pointer_listener pointer_listener = {
    pointer_enter,
    pointer_leave,
    pointer_moved,
    pointer_button_pressed,
    mouse_scrolled
};



internal void keyboard_handle_keymap(
    void *data, wl_keyboard *keyboard, uint32_t format, int fd, uint32_t size
){
    std::cout << "Keyboard handle keymap. Format " << format
	      << ", size " << size << '\n';
}

internal void keyboard_enter(
    void *data, wl_keyboard *keyboard, uint32_t serial,
    wl_surface *surface, wl_array *keys
){
    std::cout << "Keyboard gained focus\n";
}

internal void keyboard_left(
    void *data, wl_keyboard *keyboard, uint32_t serial, wl_surface *surface
) {
    std::cout << "Keyboard lost focus\n";
}

internal std::unordered_set<uint32_t> keys_pressed;
internal bool play_sound = false;

internal int frame_render_counter = 0;
internal int frame_display_counter = 0;
internal std::chrono::time_point<std::chrono::high_resolution_clock>
         start, end;

internal bool sound_played_once = true;

internal void key_pressed(
    void *data, wl_keyboard *keyboard, uint32_t serial,
    uint32_t time, uint32_t key, uint32_t state
) {
    bool is_down = state == 1;

    if (key == KEY_RIGHTALT) {
	key = KEY_LEFTALT;
    }

    if (is_down) {
	keys_pressed.insert(key);
    } else {
	keys_pressed.erase(key);
    }

    /// if current key is F4 and alt is pressed, quit
    if (key == KEY_F4
	&& keys_pressed.find(KEY_LEFTALT) != keys_pressed.end()) {
	end = std::chrono::high_resolution_clock::now();

	using std::chrono::duration_cast;
	using std::chrono::microseconds;
	using std::chrono::seconds;
	
	double time_elapsed = duration_cast<microseconds>(end - start).count();
	double time_in_seconds = duration_cast<seconds>(end - start).count();
	std::cout << "Frames Rendered: " << frame_render_counter << '\n'
		  << "Frames Displayed: " << frame_display_counter << '\n'
		  << "Time: " << time_elapsed << " microseconds\n"
		  << "Time in seconds: " << time_in_seconds << '\n'
		  << "Theoretical frame rate: " << frame_render_counter / time_in_seconds << '\n'
		  << "Actual frame rate: " << frame_display_counter / time_in_seconds << '\n';

	std::exit(0);
    }
    
    if (key == KEY_W) {
	play_sound = is_down;
	if (is_down) {
	    sound_played_once = false;
	}
    } else if (key == KEY_A) {

    } else if (key == KEY_S) {

    } else if (key == KEY_D) {

    } else if (key == KEY_Q) {

    } else if (key == KEY_E) {

    } else if (key == KEY_UP) {

    } else if (key == KEY_LEFT) {

    } else if (key == KEY_DOWN) {

    } else if (key == KEY_RIGHT) {

    } else if (key == KEY_ESC) {

    } else if (key == KEY_SPACE) {

    }      
}

internal void keyboard_handle_modifiers(
    void *data, wl_keyboard *keyboard, uint32_t serial,
    uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked,
    uint32_t group					
){
    std::cout << "Modifiers depressed: " << mods_depressed
	      << ", latched: " << mods_latched << ", locked: " << mods_locked
	      << ", group: " << group << '\n';
}

internal const wl_keyboard_listener keyboard_listener = {
    keyboard_handle_keymap,
    keyboard_enter,
    keyboard_left,
    key_pressed,
    keyboard_handle_modifiers
};


wl_keyboard *keyboard;


internal void seat_handle_capabilities(
    void *data, wl_seat *seat, uint32_t caps) {
    if (!(caps & WL_SEAT_CAPABILITY_POINTER)) {
	std::cerr << "No Mouse connected\n";
	std::exit(2);
    }

    if (!pointer) {
	pointer = wl_seat_get_pointer(seat);
	wl_pointer_add_listener(pointer, &pointer_listener, nullptr);
    }

    if (!(caps & WL_SEAT_CAPABILITY_KEYBOARD)) {
	std::cerr << "No keyboard connected\n";
	std::exit(2);
    }

    keyboard = wl_seat_get_keyboard(seat);
    wl_keyboard_add_listener(keyboard, &keyboard_listener, nullptr);
}

internal const struct wl_seat_listener seat_listener = {
    seat_handle_capabilities
};

internal void global_registry_handler(
    void *data, wl_registry *registry,
    uint32_t id, const char *interface, uint32_t version) {
    std::cout << "Got a registry event for " << interface << ' ' << id << '\n';
    
    static const std::string COMPOSITOR = "wl_compositor";
    static const std::string SHELL = "wl_shell";
    static const std::string SHM = "wl_shm";
    static const std::string SEAT = "wl_seat";
    
    if (interface == COMPOSITOR) {
	compositor = reinterpret_cast<wl_compositor*>(
	    wl_registry_bind(
	        registry,
		id,
		&wl_compositor_interface,
		1
	    )
        );
    } else if (interface == SHELL) {
	shell = reinterpret_cast<wl_shell*>(
	    wl_registry_bind(
	        registry,
		id,
		&wl_shell_interface,
		1
	    )
	);
	
    } else if (interface == SHM) {
	shm = reinterpret_cast<wl_shm*>(
	    wl_registry_bind(registry, id, &wl_shm_interface, 1)
        );
	
	wl_shm_add_listener(shm, &shm_listener, nullptr);

	wl_cursor_theme *cursor_theme = reinterpret_cast<wl_cursor_theme*>(
	    wl_cursor_theme_load(nullptr, 32, shm)
        );
	default_cursor = wl_cursor_theme_get_cursor(cursor_theme, "right_ptr");
    } else if (interface == SEAT) {
	seat = reinterpret_cast<wl_seat*>(
	    wl_registry_bind(registry, id, &wl_seat_interface, 1)
        );

	wl_seat_add_listener(seat, &seat_listener, nullptr);
    }
}

internal void global_registry_remover(void *data, wl_registry *registry, uint32_t id) {
    std::cout << "Got a registry losing event " << id << '\n';
}

/// Each pixel is 4 bytes AARRGGBB
internal const int bytes_per_pixel = 4;
struct double_buffer {
    wl_buffer *front_buffer;
    std::byte *front_data;

    wl_buffer *back_buffer;
    std::byte *back_data;
};

internal double_buffer create_double_buffer(int width, int height) {
    const char *_path = getenv("XDG_RUNTIME_DIR");
    
    /// C strings are array of bytes ending with 0.
    /// If you have "hello" its actually "hello0".
    /// If path is not nullptr, but its first element is 0,
    /// its an empty string
    if (_path == nullptr || _path[0] == '\0') {
	std::cerr << "XDG_RUNTIME_DIR not set or empty\n";
	std::exit(1);
    }

    static const std::string file_template = "/XXXXXX";
    
    std::string path = _path + file_template;   

    /// I dont know why we need O_CLOEXEC is needed. I copied
    /// this part from https://jan.newmarch.name/Wayland/SharedMemory/
    ///
    /// mkostemp takes a char*. c_str() returns const char*. Therefore,
    /// I cast away the const. We dont use path after this so, it
    /// should be fine.
    int fd = mkostemp(const_cast<char*>(path.c_str()), O_CLOEXEC);
    if (fd < 0) {
	perror("Could not create temporary file descriptor");
	std::exit(1);
    }

    unlink(path.c_str());

    int stride = width * bytes_per_pixel;
    int size_of_one_buffer = stride * height;

    /// we are allocating two buffers for double buffering
    int size = size_of_one_buffer * 2;

    /// Name is misleading. It actually resizes to size    
    if (ftruncate(fd, size * 2) < 0) {
	close(fd);
	std::cerr << "Could not resize file to size: " << size * 2 << '\n';
	std::exit(1);
    }

    std::byte *front_data = reinterpret_cast<std::byte*>(
   	mmap(
             nullptr,
             size,
             /// This seems like protect read and write but
             /// means, allow read and write in the region
             PROT_READ | PROT_WRITE,
             /// If some other process maps this same region,
             /// it will see the updates. Essentially sharing
             /// this memory. It is important to do this as
             /// we need to share this memory with wayland
             /// server
             MAP_SHARED,
             fd,
             0
        )
    );

    if (front_data == MAP_FAILED) {
	std::cerr << "Mapping Failed\n";
	std::exit(1);
    }

    /// I dont know mmap enough but calling mmap here fails with
    /// invalid arguments. Most probably because offset is not at page
    /// boundary as required.
    std::byte *back_data = front_data + size_of_one_buffer;

    if (back_data == MAP_FAILED) {
	std::perror("Map Failed for right buffer");
	std::exit(1);
    }

    wl_shm_pool *pool = wl_shm_create_pool(shm, fd, size);
    wl_buffer *front_buffer = wl_shm_pool_create_buffer(
        pool, 0,
        width, height,
        stride,
        WL_SHM_FORMAT_ARGB8888
    );
    wl_buffer *back_buffer = wl_shm_pool_create_buffer(
        pool, size_of_one_buffer,
        width, height,
        stride,
        WL_SHM_FORMAT_ARGB8888
    );

    /// Does not actually destroy the pool. The pool is destroyed when
    /// the all buffers created from pool are destroyed
    wl_shm_pool_destroy(pool);

    return {front_buffer, front_data, back_buffer, back_data};
}

internal int xoffset = 0, yoffset = 0;

internal void draw_into_buffer(int width, int height, std::byte *buffer) {
    int pitch = width * bytes_per_pixel;
    
    uint8_t *row = reinterpret_cast<uint8_t*>(buffer);
    for (int y = 0; y < height; y++) {
	uint32_t *pixel = reinterpret_cast<uint32_t*>(row);
	for (int x = 0; x < width; x++) {
	    uint8_t blue = static_cast<uint8_t>(x + xoffset);
	    uint8_t green = static_cast<uint8_t>(y + yoffset);
	    uint8_t red = 0;
	    uint8_t alpha = 0xff;

	    *pixel = (alpha << 24 | red << 16 | green << 8 | blue);
	    ++pixel;
	}

	row += pitch;
    }

    /// increase offset only when key is pressed
    if (keys_pressed.find(KEY_W) != keys_pressed.end()) {
	++xoffset;
	++yoffset;
    }

    // current_value += 0x010101;

    // // if it's reached 0xffffff (white) reset to zero
    // if (current_value > 0xffffffff) {
    // 	current_value = 0xff000000;
    // }
}

/// These need to be globals. Because draw_frame cannot
/// take these as parameters
internal wl_surface *surface;
internal const int width = 800;
internal const int height = 600;
internal double_buffer double_buffer;
internal wl_callback *frame_callback;

/// Forward declaration
internal void draw_frame(void *data, wl_callback *callback, uint32_t time);

internal const wl_callback_listener frame_listener = {
    draw_frame
};

internal void draw_frame(void *data, wl_callback *callback, uint32_t time) {
    ++frame_display_counter;
    /// How much to redraw. 0, 0, width, height means redraw everthing
    wl_surface_damage(surface, 0, 0, width, height);

    /// Apparently, the callback is only fired once. Destroy it
    /// And reinitialize the callback
    wl_callback_destroy(frame_callback);
    frame_callback = wl_surface_frame(surface);
    wl_callback_add_listener(frame_callback, &frame_listener, NULL);
    
    wl_surface_attach(surface, double_buffer.front_buffer, 0, 0);
    wl_surface_commit(surface);
}

pa_simple *sound;

const int SAMPLES_PER_SECOND = 48'000;

/// We play 48'000 samples in one frame. The square wave
/// we are using is of 256 hearts. i.e. per frame, we need
/// to play more than 187.5 samples to hear the sound. So,
/// I rounded it off to 190. If we have less than that,
/// sometimes, if I just press W and leave it, I get no sound
/// because the wave has not finished vibrating in that frame.
/// In the next frame, W is not pressed so no sound is heard.
///
/// Very important to vibrate at least once a frame for sound
/// to be heard
const int SAMPLES_IN_ONE_BUFFER = 190;


internal void init_sound() {
    /* The Sample format to use */
    static const pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = SAMPLES_PER_SECOND,
        .channels = 2
    };

    int error;
    sound = pa_simple_new(
        /// server to use. nullptr means use default
        nullptr,
	/// name of this client. Can be anything
        "Handmade Hero Clone",
	/// Open the stream for playback not recording
        PA_STREAM_PLAYBACK,
	/// which device to use. nullptr for default
        nullptr,
	/// Name of the stream
        "Handmade hero clone background music",
	/// sample specification
        &ss,
	/// channel map. I dont know what this means
        nullptr,
	/// buffering attributes. I dont know what this means.
	/// nullptr means default
	nullptr,
	/// Error is stored in this out param
        &error
    );
 
    /* Create a new playback stream */
    if (sound == nullptr) {
	std::cerr << "pa_simple_new() failed: " << pa_strerror(error) << '\n';
	/// If we cant initialize pulse audio, no big deal
	return;
    }
}

internal void destory_sound() {
    if (sound)
        pa_simple_free(sound);
 
    return;
}

int main() {
    wl_display *display = wl_display_connect(nullptr);

    if (display == nullptr) {
	std::cerr << "Can't connect to display\n";
	return 1;
    }

    std::cout << "Connectected to display\n";
    
    static const struct wl_registry_listener registry_listener = {
	global_registry_handler,
	global_registry_remover
    };

    wl_registry *registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &registry_listener, nullptr);

    wl_display_dispatch(display);
    wl_display_roundtrip(display);

    if (compositor == nullptr) {
	std::cerr << "Cant find the compositor\n";
	return 1;
    }

    std::cout << "Found compositor \n";

    surface = wl_compositor_create_surface(compositor);
    if (surface == nullptr) {
	std::cerr << "Could not create surface\n";
	return 1;
    }

    std::cout << "Created surface\n";

    if (shell == nullptr) {
	std::cerr << "Couldnt find a Wayland Shell\n";
	return 1;
    }

    shell_surface = wl_shell_get_shell_surface(
        shell,
        surface);
    if (shell_surface == nullptr) {
	std::cerr << "Could not create shell surface\n";
	return 1;
    }

    std::cout << "Created shell surface\n";

    wl_shell_surface_set_toplevel(shell_surface);
    wl_shell_surface_add_listener(
        shell_surface, &shell_surface_listener, nullptr
    );

    double_buffer = create_double_buffer(width, height);

    frame_callback = wl_surface_frame(surface);
    
    wl_callback_add_listener(frame_callback, &frame_listener, nullptr);

    cursor_surface = wl_compositor_create_surface(compositor);

    draw_into_buffer(width, height, double_buffer.front_data);
    draw_frame(nullptr, nullptr, 0);

    init_sound();

    int wave_counter = 0;
    int hz = 256;    
    int wave_period = SAMPLES_PER_SECOND / hz;
    int error;
    int samples_played = 0;

    int display_fd = wl_display_get_fd(display);

    pollfd pfd = {
	/// The file descriptor to observe
	display_fd,
	/// Events to watch for. Look at man page for more info
	POLLIN | POLLPRI | POLLOUT | POLLRDHUP,
	/// There is also another field revents which is out parameter.
	/// We dont need to set that. Kernel returns the events in
	/// revents when we call poll
    };

    start = std::chrono::high_resolution_clock::now();

    while (true) {
	++frame_render_counter;
	/// This loop does not block if there are no events.
	/// The older loop uses wl_display_dispatch which
	/// blocked if there were no events which we could not do because
	/// we want to play music in background which is hard realtime.       
	///
	/// https://wayland.freedesktop.org/docs/html/apb.html#Client-classwl__display_1a40039c1169b153269a3dc0796a54ddb0
	while (wl_display_prepare_read(display) != 0) {
	    wl_display_dispatch_pending(display);
	}

	wl_display_flush(display);

	/// poll takes an array of poll_fd i.e. a pointer and a size.
	/// &pfd is the pointer, 1 is size of array. 0 means dont wait
	/// for the event to occur
	int ret = poll(&pfd, 1, 0);

	if (ret < 0) {
	    wl_display_cancel_read(display);
	    std::perror("Poll failed");
	    continue;
	}

	wl_display_read_events(display);
	wl_display_dispatch_pending(display);

	draw_into_buffer(width, height, double_buffer.front_data);
	
	/// For next frame draw into the back buffer. 
	std::swap(double_buffer.front_data, double_buffer.back_data);
	std::swap(double_buffer.front_buffer, double_buffer.back_buffer);

	/// If sound is played once, check play_sound. Otherwise,
	/// play the sound at least once
	if (sound_played_once) {
	    if (!play_sound) {
		continue;
	    }
	}

	sound_played_once = true;

	int16_t buff[SAMPLES_IN_ONE_BUFFER*2];

	for (int i = 0; i < sizeof(buff) / sizeof(uint16_t); i += 2) {
	    float t = 2 * PI * samples_played / wave_period;
	    float sin_value = sinf(t);
	    int value = static_cast<int>(sin_value * 16000);

	    buff[i] = value;
	    buff[i+1] = value;
	    --wave_counter;
	    ++samples_played;
	}	        
 
        if (pa_simple_write(sound, buff, sizeof(buff), &error) < 0) {
	    std::cerr << "pa_simple_write() failed: " << pa_strerror(error)
		      << '\n';
        }
    }

    /* Make sure that every single sample was played */
    if (pa_simple_drain(sound, &error) < 0) {
	std::cerr << "pa_simple_drain() failed: " << pa_strerror(error)
		  << '\n';
    }

    wl_display_disconnect(display);

    std::cout << "Disconnected from display\n";
}
